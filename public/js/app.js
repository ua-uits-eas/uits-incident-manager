'use strict';

// Declare app level module which depends on filters, and services
angular.module('myApp', ['ngRoute', 'myApp.filters', 'myApp.services', 'myApp.directives', 'monospaced.elastic', 'ui.bootstrap', 'ngSanitize', 'ngNotify']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
      templateUrl: 'partials/index',
      controller: IndexCtrl
    })
    .when('/i/:id', {
      templateUrl: '/partials/incident',
      controller: GetIncidentCtrl
    })
    .when('/new-alert', {
      templateUrl: 'partials/new-alert',
      controller: AddIncidentCtrl
    })
    .when('/logout', {
        templateUrl: 'partials/logout',
        controller: LogoutCtrl
    })
    .when('/subscribe', {
      templateUrl: 'partials/subscribe',
      controller: SubscribeCtrl
    })
    .when('/admin-login', {
      templateUrl: 'partials/admin-login',
      controller: AdminLoginCtrl
    })
    .when('/admin-login-err', {
      templateUrl: 'partials/admin-login',
      controller: AdminLoginCtrl
    })
    .otherwise({
      redirectTo: '/'
    });

    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
  }]);
