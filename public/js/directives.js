'use strict';

/* Directives */


angular.module('myApp.directives', []).
directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])
  .directive('dropdown', function() {
    return function(scope, elm, attrs) {
      $(elm).dropdown();
    };
  })
  // GDW -- 2015/11/16 -- this is to allow alert checkbox on incident form to not trigger dirty state
  .directive('noDirtyCheck', [function() {
      return {
        restrict: "A",
        require: "ngModel",
        link: function(scope, elem, attrs, ngModelCtrl) {
          if (!ngModelCtrl) {
            return;
          }

          var clean = (ngModelCtrl.$pristine && !ngModelCtrl.$dirty);

          if (clean) {
            ngModelCtrl.$pristine = false;
            ngModelCtrl.$dirty = true;
          }
        }
      };
    }]);
