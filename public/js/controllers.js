'use strict';

/* Controllers */


/*function NavBarController($scope) {
    $scope.onTwitterLogin = function()
    {
        // a direct window.location to overcome Angular intercepting your call!
        window.location = "/auth/twitter";
    }

    $scope.onFacebookLogin = function () {

    }
}*/

function IndexCtrl($scope, $http, $route, $routeParams, $location) {
  $scope.incidents = [];
  $scope.item = {};

  // allows us to specify an Angular route externally
  // (e.g., in order to pass error messages from Connect/Express)
  var route = $routeParams.route;
  if (route) {
    if ($location.$$search.route) {
      delete $location.$$search.route;
      $location.$$compose();
    }
    $location.path(route);
    return;
  }

  $http({
    method: 'GET',
    url: '/api/incidentList'
  }).
  success(function(data, status, headers, config) {
    $scope.incidents = data.incidents
    $scope.isAdmin = data.isAdmin;
    for (var i = 0; i < $scope.incidents.length; i++) {
      $scope.incidents[i].create_time = new Date(Date.parse($scope.incidents[i].create_time)).toLocaleString();
      $scope.incidents[i].modify_time = new Date(Date.parse($scope.incidents[i].modify_time)).toLocaleString();
    }
  });
}

/*function LoginCtrl($scope,$location)
{
    $scope.onLoginClick = function()
    {
        // a direct window.location to overcome Angular intercepting your call!
        window.location = "/auth/twitter";
    }
}
*/

function AdminLoginCtrl($scope, $http, $location, $routeParams, ngNotify) {
  $scope.username = '';
  if ('message' in $routeParams) {
    ngNotify.set($routeParams.message, {
      duration: 10000,
      type: 'error'
    });
  }

  $scope.submitLogin = function(isValid) {
    if (isValid) {
      $http.post('/admin-login-submit', {
        username: $scope.username
      });
    }
  };
}

function LogoutCtrl($scope, $location) {
  window.location = "/logout";
}

function GetIncidentCtrl($scope, $http, $location, $routeParams, $anchorScroll, ngNotify) {
  $scope.update = '';
  $scope.addStatus = false;
  $scope.displayUnregisterPopup = false;

  $http({
    method: 'GET',
    url: '/api/i/' + $routeParams.id
  }).
  success(function(data, status, headers, config) {
    if (data === false) {
      ngNotify.set('Could not find incident ' + $routeParams.id, {
        duration: 10000,
        type: 'error'
      });
      $location.path('/');
    } else {
      $scope.isAdmin = data.isAdmin;
      $scope.id = data.incident['id'];
      $scope.title = data.incident['title'];
      $scope.status = data.incident['status'];
      $scope.description = data.incident['description'];
      $scope.services = data.incident['services'];
      $scope.impact = data.incident['impact'];
      $scope.planop = data.incident['planop'];
      $scope.contact = data.incident['contact'];
      $scope.timeframe = data.incident['timeframe'];
      $scope.updates = data.incident['updates'];
      $scope.send_alert = true;
      for (var i = 0; i < $scope.updates.length; i++) {
        $scope.updates[i].create_time = new Date(Date.parse($scope.updates[i].create_time)).toLocaleString();
      }
      $scope.auditlog = data.incident['auditlog'];
      for (var i = 0; i < $scope.auditlog.length; i++) {
        $scope.auditlog[i].create_time = new Date(Date.parse($scope.auditlog[i].create_time)).toLocaleString();
      }
      $scope.create_time = new Date(Date.parse(data.incident['create_time'])).toLocaleString();
      $scope.modify_time = new Date(Date.parse(data.incident['modify_time'])).toLocaleString();
    }
  });

  $scope.submitIncident = function(isValid) {
    if (isValid) {
      $http.post('/api/incident-update', {
        id: $scope.id,
        title: $scope.title,
        status: $scope.status,
        description: $scope.description,
        services: $scope.services,
        impact: $scope.impact,
        planop: $scope.planop,
        contact: $scope.contact,
        timeframe: $scope.timeframe,
        update: $scope.update,
        send_alert: $scope.send_alert ? "yes" : "no"
      }).
      success(function(data, status, headers, config) {
        if (!data.result.status) {
          ngNotify.set(data.result.error, {
            duration: 10000,
            type: 'error'
          });
        } else {
          ngNotify.set('Incident successfully updated', {
            type: 'success',
            duration: 10000
          });
        }
        $location.path('/');
      });
    }
  };
  $scope.unregisterIncident = function() {
    $http.post('/api/incident-del', {
      id: $scope.id
    }).
    success(function(data, status, headers, config) {
      if (!data.result.status) {
        ngNotify.set(data.result.error, {
          duration: 10000,
          type: 'error'
        });
      }
      $location.path('/');
    });
  };
  $scope.showUnregisterPopup = function(options) {
    if (options === true) {
      $scope.displayUnregisterPopup = true;
      $anchorScroll();
    } else {
      $scope.displayUnregisterPopup = false;
    }
  };
}

function AddIncidentCtrl($scope, $http, $location, $routeParams, ngNotify) {
  $scope.title = '';
  $scope.status = 'OPEN';
  $scope.description = '';
  $scope.services = '';
  $scope.impact = '';
  $scope.planop = '';
  $scope.contact = '';
  $scope.timeframe = '';

  $scope.save = function(model) {
    if (!$scope.title.$valid) {
      $scope.title.submitted = true;
    }
    if (!$scope.description.$valid) {
      $scope.description.submitted = true;
    }
    if (!$scope.services.$valid) {
      $scope.services.submitted = true;
    }
    if (!$scope.impact.$valid) {
      $scope.impact.submitted = true;
    }
    if (!$scope.planop.$valid) {
      $scope.planop.submitted = true;
    }
    if (!$scope.contact.$valid) {
      $scope.contact.submitted = true;
    }
    if (!$scope.timeframe.$valid) {
      $scope.timeframe.submitted = true;
    }
  };

  $scope.submitList = function(isValid) {
    if (isValid) {
      $http.post('/api/incident-add', {
        title: $scope.title,
        status: $scope.status,
        description: $scope.description,
        services: $scope.services,
        impact: $scope.impact,
        planop: $scope.planop,
        contact: $scope.contact,
        timeframe: $scope.timeframe
      }).
      success(function(data, status, headers, config) {
        if (!data.result.status) {
          ngNotify.set(data.result.error, {
            duration: 10000,
            type: 'error'
          });
        }
        $location.path('/');
      });
    }
  };
}

function SubscribeCtrl($scope, $http, $location, $route, $routeParams, ngNotify) {
  $scope.subscriptions = [];
  $scope.item = {};
  $scope.addSub = false;
  $scope.sub_type = "EMAIL";
  $scope.email = "";
  $scope.phone = "";

  $scope.phoneRE = /^\d{10}$/;

  $scope.refreshSubs = function() {
    $http({
      method: 'GET',
      url: '/api/subscriptions'
    }).
    success(function(data, status, headers, config) {
      if (data == false) {
        ngNotify.set('Unknown Error', {
          duration: 10000,
          type: 'error'
        });
      } else {
        $scope.subscriptions = data.subscriptions;
        $scope.isAuth = data.isAuth;
        for (var i = 0; i < $scope.subscriptions.length; i++) {
          $scope.subscriptions[i].create_time = new Date(Date.parse($scope.subscriptions[i].create_time)).toLocaleString();
        }
        if (!$scope.isAuth) {
          ngNotify.set('Authorization Denied', {
            duration: 10000,
            type: 'error'
          });
        }
      }
    });
  };

  $scope.subscribeEndpoint = function(isValid) {
    if (!$scope.isAuth) {
      ngNotify.set('Authorization Denied', {
        duration: 10000,
        type: 'error'
      });
      return false;
    }
    if (isValid) {
      $http.post('/api/subscribe', {
        type: $scope.sub_type,
        endpoint: ($scope.sub_type == "EMAIL") ? $scope.email : "1" + $scope.phone
      }).
      success(function(data, status, headers, config) {
        if (!data.result.status) {
          ngNotify.set(data.result.error, {
            duration: 10000,
            type: 'error'
          });
        } else {
          ngNotify.set('Your subscription request has been submitted.<br><b>NOTE:</b> you will receive a confirmation ' + ($scope.sub_type == "EMAIL" ? 'email from <b>UITSAlerts &lt;no-reply@sns.amazonaws.com&gt;</b>' : 'SMS message from <b>UITSALERTS (303-04)</b>') + " with instructions to confirm your subscription.<br>You <b>must</b> confirm your subscription in order to receive alerts.", {
            type: 'success',
            html: true,
            duration: 30000,
            button: true
          });
          $scope.refreshSubs();
          $scope.addSub = false;
          $scope.sub_type = 'EMAIL'
          $scope.email = '';
          $scope.phone = '';
          $scope.subForm.$setPristine();
        }
      });
    }
  };

  $scope.unsubscribeEndpoint = function(arn, sub_id) {
    if (!$scope.isAuth) {
      ngNotify.set('Authorization Denied', {
        duration: 10000,
        type: 'error'
      });
      return false;
    }

    $http.post('/api/unsubscribe', {
      arn: arn,
      sub_id: sub_id,
    }).
    success(function(data, status, headers, config) {
      if (!data.result.status) {
        ngNotify.set(data.result.error, {
          duration: 10000,
          type: 'error'
        });
      } else {
        $scope.refreshSubs();
      }
    });
  }

  $scope.refreshSubs();
}
