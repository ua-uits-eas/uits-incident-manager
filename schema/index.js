// our DynamoDB schema

var shortid = require('shortid');
var uuid = require('node-uuid');

// Incident table
var IncidentSchemaDef = {
  id: {
    type: String,
    hashKey: true,
    default: function() {
      return shortid.generate();
    }
  },
  title: {
    type: String,
    trim: true,
    required: true,
  },
  status: {
    type: String,
    required: true,
    validate: function(v) {
      return (v == "OPEN" || v == "CLOSED" || v == "DELETED");
    },
  },
  description: {
    type: String,
    required: true,
  },
  services: {
    type: String,
    required: true,
  },
  impact: {
    type: String,
    required: true,
  },
  planop: {
    type: String,
    required: true,
  },
  contact: {
    type: String,
    required: true,
  },
  timeframe: {
    type: String,
    required: true,
  },
  create_time: {
    type: Date,
    required: true,
    default: function() {
      var modified = new Date();
      return modified.getTime();
    },
  },
  modify_time: {
    type: Date,
    required: true,
    default: function() {
      var modified = new Date();
      return modified.getTime();
    },
  },
};

// IncidentUpdates table
var IncidentUpdatesSchemaDef = {
  id: {
    type: String,
    hashKey: true,
  },
  update_id: {
    type: String,
    required: true,
    rangeKey: true,
    default: function() {
      return uuid.v4();
    },
  },
  updated_by: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  create_time: {
    type: Date,
    required: true,
    default: function() {
      var modified = new Date();
      return modified.getTime();
    },
  },
};

// IncidentChangeLog table
var IncidentChangeLogSchemaDef = {
  id: {
    type: String,
    hashKey: true,
  },
  change_id: {
    type: String,
    required: true,
    rangeKey: true,
    default: function() {
      return uuid.v4();
    },
  },
  changed_by: {
    type: String,
    required: true,
  },
  changes: {
    type: 'list',
    list: [
      {
        field: { type: String, required: true },
        prev_val: { type: String, required: true },
        new_val: { type: String, required: true }
      }
    ]
  },
  create_time: {
    type: Date,
    required: true,
    default: function() {
      var modified = new Date();
      return modified.getTime();
    },
  },
};

// IncidentSubscription table
var IncidentSubscriptionSchemaDef = {
  netid: {
    type: String,
    hashKey: true,
  },
  sub_id: {
    type: String,
    required: true,
    rangeKey: true,
    default: function() {
      return uuid.v4();
    },
  },
  endpoint_type: {
    type: String,
    required: true,
    validate: function(v) {
      return (v == "EMAIL" || v == "SMS");
    }
  },
  endpoint: {
    type: String,
    required: true,
  },
  create_time: {
    type: Date,
    required: true,
    default: function() {
      var modified = new Date();
      return modified.getTime();
    },
  },
};

// IncidentAdmin table
var IncidentAdminSchemaDef = {
  user: {
    type: String,
    hashKey: true,
  }
};

exports.IncidentSchemaDef = IncidentSchemaDef;
exports.IncidentUpdatesSchemaDef = IncidentUpdatesSchemaDef;
exports.IncidentChangeLogSchemaDef = IncidentChangeLogSchemaDef;
exports.IncidentSubscriptionSchemaDef = IncidentSubscriptionSchemaDef;
exports.IncidentAdminSchemaDef = IncidentAdminSchemaDef;
