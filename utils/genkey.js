//////////////////////////////////////////////////////////
// Use this to generate a base64-encoded encryption key
// and encrypt Shib key and Express session secret

var crypto = require('crypto');
var node_cryptojs = require('node-cryptojs-aes');
var fs = require('fs');

if (process.argv.length != 4) {
  console.error("Usage: node " + process.argv[1] + " <path to Shib private key file> <path to session secret file>");
  process.exit(1);
}

// node-cryptojs-aes main object;
var CryptoJS = node_cryptojs.CryptoJS;

// custom json serialization format
var JsonFormatter = node_cryptojs.JsonFormatter;

// generate random passphrase binary data
var r_pass = crypto.randomBytes(128);

// convert passphrase to base64 format
var r_pass_base64 = r_pass.toString("base64");

console.log("passphrase base64 format (this needs to be placed in DECRYPTKEY environment var): ");
console.log(r_pass_base64);

// message to cipher
if (fs.existsSync(process.argv[2])) {
  var shibKey = fs.readFileSync(process.argv[2], 'utf-8');
  var encrypted = CryptoJS.AES.encrypt(shibKey, r_pass_base64, { format: JsonFormatter });
  fs.writeFileSync(process.argv[2] + ".enc", encrypted.toString());
  console.log("Encrypted version of " + process.argv[2] + " saved as " + process.argv[2] + ".enc");
} else {
  console.error("Could not open file: " + process.argv[2])
  process.exit(1);
}
if (fs.existsSync(process.argv[3])) {
  var sessionSecret = fs.readFileSync(process.argv[3], 'utf-8');
  var encrypted = CryptoJS.AES.encrypt(sessionSecret, r_pass_base64, { format: JsonFormatter });
  fs.writeFileSync(process.argv[3] + ".enc", encrypted.toString());
  console.log("Encrypted version of " + process.argv[3] + " saved as " + process.argv[3] + ".enc");
} else {
  console.error("Could not open file: " + process.argv[3])
  process.exit(1);
}

process.exit(0);
