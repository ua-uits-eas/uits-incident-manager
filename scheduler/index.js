/*
 * scheduled tasks
 */

var _ = require('underscore');
var moment = require('moment-timezone');
var dynamoose = require('dynamoose');
var async = require('async');
var AWS = require('aws-sdk');
var Twitter = require('twitter');
var ldap = require('ldapjs');
var sets = require('simplesets');
var schema = require('../schema');
var debug = require('debug')('scheduler');

var IncidentAdminSchema = new dynamoose.Schema(schema.IncidentAdminSchemaDef);

// poll SQS queue and deliver notifications to @UITS_Alerts Twitter account
exports.pollSQSForTwitter = function(sqsQueueUrl, twitConsumerKey, twitConsumerSecret, twitAccessTokenKey, twitAccessTokenSecret) {
  debug("*** Starting SQS poll...");

  // Instantiate SQS client
  var sqs = new AWS.SQS();

  // use named function for final waterfall callback, so we can short-circuit
  // tweet step if there are no pending messages
  var finalCallback = function(err) {
    if (err) {
      logger.error(err.text, err.details);
    }
  };

  async.waterfall([
      // poll SQS
      function(callback) {
        sqs.receiveMessage({
          QueueUrl: sqsQueueUrl,
          MaxNumberOfMessages: 1, // how many messages do we wanna retrieve?
          VisibilityTimeout: 60, // seconds - how long we want a lock on this job
          WaitTimeSeconds: 3 // seconds - how long should we wait for a message?
        }, function(err, data) {
          if (err) {
            debug("*** err = " + JSON.stringify(err));
            debug("*** data = " + JSON.stringify(data));
            return finalCallback({
              text: "Could not poll SQS",
              details: {
                err: err
              }
            });
          } else {
            // If there are any messages to get
            if (data.Messages) {
              // Get the first message (should be the only one since we said to only get one above)
              var message = data.Messages[0],
                body = JSON.parse(message.Body);
              // Now this is where you'd do something with this message
              return callback(null, body.Subject, message.ReceiptHandle);
            } else {
              return finalCallback(null);
            }
          }
        });
      },
      // send tweet if there is a message
      function(tweet, receipt, callback) {
        var client = new Twitter({
          consumer_key: twitConsumerKey,
          consumer_secret: twitConsumerSecret,
          access_token_key: twitAccessTokenKey,
          access_token_secret: twitAccessTokenSecret
        });
        client.post('statuses/update', {
          status: tweet + ' (' + moment().tz('America/Phoenix').format('MM/DD/YYYY, h:mm:ss a') + ')'
        }, function(error, _tweet, response) {
          if (error) {
            return finalCallback({
              text: "Could not submit tweet",
              details: {
                tweet: tweet,
                err: error
              }
            });
          } else {
            debug("*** Tweet = " + JSON.stringify(_tweet));
            return callback(null, receipt);
          }
        });
      },
      // delete SQS message if tweet sent successfully
      function(receipt, callback) {
        sqs.deleteMessage({
          QueueUrl: sqsQueueUrl,
          ReceiptHandle: receipt // receipt handle for message to delete
        }, function(err, data) {
          if (err) {
            debug("*** err = " + JSON.stringify(err));
            debug("*** data = " + JSON.stringify(data));
            return finalCallback({
              text: "Could not delete SQS message",
              details: {
                err: err
              }
            });
          } else {
            return finalCallback(null);
          }
        });
      }
    ],
    finalCallback
  );
}

// get list of admin users and sync IncidentAdmin table
exports.syncAdmins = function(adminGroupName, ldapUrl, ldapBindDN, ldapBindPw, ldapSearchBase) {
  var dbSet = new sets.Set();
  var edsSet = new sets.Set();
  var IncidentAdmin = dynamoose.model(tblpfx + 'IncidentAdmin', IncidentAdminSchema);

  async.series([
      // get Grouper group members and resolve NetID usernames
      function(callback) {
        // set up LDAP connection
        var ldapc = ldap.createClient({
          url: ldapUrl
        });
        async.series([
            // 1. Bind to EDS LDAP
            function(callback) {
              ldapc.bind(ldapBindDN, ldapBindPw, function(err) {
                if (err) {
                  return callback(new Error("Cannot bind to LDAP: " + err.message));
                } else {
                  debug("Bound to EDS");
                  return callback(null);
                }
              });
            },
            function(callback) {
              // lookup Grouper group members
              var ldapOpts = {
                filter: '(ismemberof=' + adminGroupName + ')',
                attributes: ['uid'],
                scope: 'sub',
                timeLimit: 300
              };
              ldapc.search(ldapSearchBase, ldapOpts, function(err, res) {
                if (err) return callback(err);
                res.on('searchEntry', function(entry) {
                  for (var i = 0; i < entry.attributes.length; i++) {
                    switch (entry.attributes[i].type.toLowerCase()) {
                      case "uid":
                        edsSet.add(entry.attributes[i].vals[0]);
                        break;
                    }
                  }
                });
                res.on('end', function(result) {
                  ldapc.unbind(function(err) {
                    if (err) return callback(err);
                    return callback()
                  });
                });
                res.on('error', function(err) {
                  console.error("Error in LDAP search: " + err.message);
                  return callback(err);
                });
              });
            }
          ],
          function(err) {
            if (err) {
              debug("*** Error in LDAP search for admin group members: " + JSON.stringify(err));
              return callback(err);
            }
            debug("**** EDS Set = " + JSON.stringify(edsSet.array()));
            return callback(null);
          }
        );
      },
      function(callback) {
        // get group membership from DynamoDB
        IncidentAdmin.scan().exec(function(err, mems) {
          if (err)
            callback(err);
          if (mems.length > 0) {
            _.each(mems, function(item) {
              dbSet.add(item.user);
            });
          };
          debug("**** DynamoDB set = " + JSON.stringify(dbSet.array()));
          callback(null);
        });
      },
      function(callback) {
        // delete members from DynamoDB who aren't in LDAP group
        async.each(dbSet.difference(edsSet).array(),
          function(item, callback) {
            IncidentAdmin.delete({
              user: item
            }, function(err) {
              if (err) return callback(new Error("Could not delete admin user " + item + ": " + err.message));
              return callback(null);
            });
          },
          function(err) {
            if (err) return callback(err);
            return callback(null);
          }
        );
      },
      function(callback) {
        // add members to DynamoDB who are in LDAP group but not in DB
        async.each(edsSet.difference(dbSet).array(),
          function(item, callback) {
            new IncidentAdmin({
              user: item
            }).save(
              function(err) {
                if (err) return callback(new Error("Could not create admin user " + item + ": " + err.message));
                return callback(null);
              }
            );
          },
          function(err) {
            if (err) return callback(err);
            return callback(null);
          }
        );
      }
    ],
    function(err, results) {
      if (err) {
        logger.error(err.text, err.details);
      }
    }
  );
}
