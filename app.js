/**
 * Module dependencies.
 */

const loginUrl = '/login';
const loginCallbackUrl = '/login/callback';
const logoutUrl = '/logout';

var http = require('http'); //http server
var https = require('https'); //https server
var fs = require('fs'); //file system
var node_cryptojs = require('node-cryptojs-aes'); // AES encryption library
var express = require('express'); //express middleware
var morgan = require('morgan'); //logger for express
var bodyParser = require('body-parser'); //body parsing middleware
var cookieParser = require('cookie-parser'); //cookie parsing middleware
var session = require('express-session'); //express session management
var passport = require('passport'); //authentication middleware
var uashib = require('passport-uashib'); //UA Shibboleth auth strategy
var local_auth = require('passport-local-optional-password'); // local strategy for getting NetID for Duo Admin Auth
var duo_auth = require('passport-duo');
var loggedin = require('connect-ensure-login');
var AWS = require('aws-sdk');
var dynamoose = require('dynamoose');
var async = require('async');
var flash = require('connect-flash');
var routes = require('./routes');
var api = require('./routes/api');
var schema = require('./schema');
var scheduler = require('./scheduler');
var debug = require('debug')('http');

///////////////////////////////////////////////////////////////////////////////
// load files and read environment variables
//

//get entityId and domain from environment variable
//this is necessary as the passport-saml library requires
//this when we create the Strategy
var entityId = process.env.ENTITYID;
if (!entityId || entityId.length == 0)
  throw new Error('You must specify the Shibboleth entityId of this server via the ENTITYID environment variable!');

var domain = process.env.DOMAIN;
if (!domain || domain.length == 0)
  throw new Error('You must specify the domain of this server via the DOMAIN environment variable!');

//uncomment the following if handling SSL within Node
var httpsPort = process.env.HTTPSPORT || 443;

// base URL for sending out alert links
var alertBaseUrl = process.env.ALERTBASEURL || 'https://incidents.uits.arizona.edu:4443/i/';

// Bit.ly API key (for shortening URLs)
var bitlyApiKey = process.env.BITLYAPIKEY;
if (!bitlyApiKey || bitlyApiKey.length == 0) {
  throw new Error('You must specify the Bit.ly API key via the BITLYAPIKEY environment variable!');
}

// AWS SNS Topic ARN
var snsTopicArn = process.env.AWSSNSTOPICARN;
if (!snsTopicArn || snsTopicArn.length == 0) {
  throw new Error('You must specify the SNS Topic ARN via the AWSSNSTOPICARN environment variable!');
}

// AWS SQS URL
var sqsUrl = process.env.AWSSQSURL;
if (!sqsUrl || sqsUrl.length == 0) {
  throw new Error('You must specify the SQS URL via the AWSSQSURL environment variable!');
}

// Twitter API credentials/tokens
var twitConsumerKey = process.env.TWITTERCONSUMERKEY;
if (!twitConsumerKey || twitConsumerKey.length == 0) {
  throw new Error('You must specify the Twitter Consumer API key via the TWITTERCONSUMERKEY environment variable!');
}
var twitConsumerSecret = process.env.TWITTERCONSUMERSECRET;
if (!twitConsumerSecret || twitConsumerSecret.length == 0) {
  throw new Error('You must specify the Twitter Consumer API secret via the TWITTERCONSUMERSECRET environment variable!');
}
var twitAccessTokenKey = process.env.TWITTERACCESSTOKENKEY;
if (!twitAccessTokenKey || twitAccessTokenKey.length == 0) {
  throw new Error('You must specify the Twitter Access Token key via the TWITTERACCESSTOKENKEY environment variable!');
}
var twitAccessTokenSecret = process.env.TWITTERACCESSTOKENSECRET;
if (!twitAccessTokenSecret || twitAccessTokenSecret.length == 0) {
  throw new Error('You must specify the Twitter Access Token secret via the TWITTERACCESSTOKENSECRET environment variable!');
}

// S3 bucket containing Shib and session secrets
var s3bucket = process.env.S3BUCKET;
if (!s3bucket || s3bucket.length == 0) {
  throw new Error('You must specify the S3 bucket name via the S3BUCKET environment variable!');
}

// S3 bucket containing Shib and session secrets
var s3folder = process.env.S3FOLDER || "";

// decryption secret for Shib private key and Connect session secret
var decryptKey = process.env.DECRYPTKEY;
if (!decryptKey || decryptKey.length == 0) {
  throw new Error('You must specify the decryption key via the DECRYPTKEY environment variable!');
}

// DynamoDB table name prefix
tblpfx = process.env.DYNAMODBTBLPREFIX; // global
if (!tblpfx || tblpfx.length == 0) {
  throw new Error('You must specify the Dynamo DB table name prefix via the DYNAMODBTBLPREFIX environment variable!');
}

// this is populated from EDS/Grouper by setInterval() task
var adminGroupName = process.env.ADMINGROUP;
if (!adminGroupName || adminGroupName.length == 0) {
  throw new Error('You must specify the Grouper admin group name via the ADMINGROUP environment variable!');
}
var ldapUrl = process.env.LDAPURL;
if (!ldapUrl || ldapUrl.length == 0) {
  throw new Error('You must specify the LDAP URL the LDAPURL environment variable!');
}
var ldapBindDN = process.env.LDAPBINDDN;
if (!ldapBindDN || ldapBindDN.length == 0) {
  throw new Error('You must specify the LDAP BIND DN via the LDAPBINDDN environment variable!');
}
var ldapBindPw = process.env.LDAPBINDPW;
if (!ldapBindPw || ldapBindPw.length == 0) {
  throw new Error('You must specify the LDAP BIND password via the LDAPBINDPW environment variable!');
}
var ldapSearchBase = process.env.LDAPSEARCHBASE;
if (!ldapSearchBase || ldapSearchBase.length == 0) {
  throw new Error('You must specify the LDAP search base via the LDAPSEARCHBASE environment variable!');
}

// Logging middleware for Node...we will log to AWS CloudWatch
var logGroup = process.env.AWSCWLOGGRP;
if (!logGroup || logGroup.length == 0) {
  throw new Error('You must specify the AWS CloudWatch log group name via the AWSCWLOGGRP environment variable!');
}
var logStream = process.env.AWSCWLOGSTRM;
if (!logStream || logStream.length == 0) {
  throw new Error('You must specify the AWS CloudWatch log stream name via the AWSCWLOGSTRM environment variable!');
}

// Duo passport strategy variables
var duoIkey = process.env.DUOIKEY;
if (!duoIkey || duoIkey.length == 0) {
  throw new Error('You must specify the Duo Integration Key via the DUOIKEY environment variable!');
}
var duoSkey = process.env.DUOSKEY;
if (!duoSkey || duoSkey.length == 0) {
  throw new Error('You must specify the Duo Secret Key via the DUOSKEY environment variable!');
}
var duoHost = process.env.DUOHOST;
if (!duoHost || duoHost.length == 0) {
  throw new Error('You must specify the Duo FQDN hostname via the DUOHOST environment variable!');
}

// global
logger = require('winston'),
  options = {
    logGroupName: logGroup,
    logStreamName: logStream,
  };
logger.add(require('winston-cloudwatch'), options);

// for decrypting key & secret
// node-cryptojs-aes main object;
var CryptoJS = node_cryptojs.CryptoJS;

// custom json serialization format
var JsonFormatter = node_cryptojs.JsonFormatter;

// load SAML public certificate and private key
// from S3, along with Express session secret
async.parallel([
    function(callback) {
      var s3 = new AWS.S3();
      var params = {
        Bucket: s3bucket,
        Key: s3folder + 'server-pvk.pem.enc'
      };
      s3.getObject(params, function(err, data) {
        if (err) {
          return callback(err); // an error occurred
        } else {
          return callback(null, data.Body.toString());
        };
      });
    },
    function(callback) {
      var s3 = new AWS.S3();
      var params = {
        Bucket: s3bucket,
        Key: s3folder + 'server-cert.pem'
      };
      s3.getObject(params, function(err, data) {
        if (err) {
          return callback(err); // an error occurred
        } else {
          return callback(null, data.Body.toString());
        };
      });
    },
    function(callback) {
      var s3 = new AWS.S3();
      var params = {
        Bucket: s3bucket,
        Key: s3folder + 'session-secret.txt.enc'
      };
      s3.getObject(params, function(err, data) {
        if (err) {
          return callback(err); // an error occurred
        } else {
          return callback(null, data.Body.toString());
        };
      });
    }
  ],
  function(err, results) {
    if (err) {
      throw new Error('Error retrieving S3 files: ' + err.message);
    } else {
      var publicCert = results[1];
      var privateKeyDec = CryptoJS.AES.decrypt(results[0], decryptKey, {
        format: JsonFormatter
      });
      var privateKey = CryptoJS.enc.Utf8.stringify(privateKeyDec);
      var secretDec = CryptoJS.AES.decrypt(results[2], decryptKey, {
        format: JsonFormatter
      });
      var secret = CryptoJS.enc.Utf8.stringify(secretDec);

      // main app object
      var app = module.exports = express();

      // Configuration
      app.set('views', __dirname + '/views');
      app.engine('.html', require('ejs').renderFile);
      app.set('view engine', 'html');
      app.set('view options', {
        layout: false
      });
      // AWS EB sets PORT environment var
      app.set('port', process.env.PORT || 8081);
      // trust EBS nginx proxy
      app.set('trust proxy', true);

      app.use(morgan({
        format: process.env.LOGFORMAT || 'dev'
      }));
      app.use(bodyParser.urlencoded({
        extended: true
      }));
      app.use(bodyParser.json({
        type: 'application/json'
      }));
      app.use(cookieParser());
      app.use(session({
        secret: secret,
        cookie: {
          secret: true
        }
      }));

      ///////////////////////////////////////////////////////////////////////////////
      // redirect HTTP to HTTPS
      //
      app.use(function requireHTTPS(req, res, next) {
        //----------------------------
        // Insecure request?
        //----------------------------
        if (req.get('x-forwarded-proto') == 'http') {
          //----------------------------
          // Redirect to https://
          //----------------------------
          return res.redirect('https://' + req.get('host') + req.url);
        }
        next();
      });

      app.use(flash());
      app.use(passport.initialize());
      app.use(passport.session());
      app.use(app.router);
      app.use(express.static(__dirname + '/public'));

      // app.configure('production', function(){
      //   app.use(express.errorHandler());
      // });
      app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
      }));

      // enable this in production
      //console.log = function() {};

      // middleware function to disable browser-side cacheing
      function nocache(req, res, next) {
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
        res.header('Expires', '-1');
        res.header('Pragma', 'no-cache');
        next();
      }

      //create authorization function to pass to UA Shibboleth Strategy
      var authzFunc = function(attrs) {
        result = {};
        result.status = false;

        if ("Shib-uid" in attrs) {
          result.status = true;
        }
        if (!result.status) {
          result.message = 'You are not authorized to access this resource';
        }
        return result;
      }

      passport.serializeUser(function(user, done) {
        done(null, user);
      });
      passport.deserializeUser(function(user, done) {
        done(null, user);
      });

      //create the UA Shibboleth Strategy and tell Passport to use it
      var shibStrategy = new uashib.Strategy({
        entityId: entityId,
        privateKey: privateKey,
        callbackUrl: loginCallbackUrl,
        authz: authzFunc,
        domain: domain
      });
      passport.use(shibStrategy);

      // setup local strategy for admin users to bypass Shib (e.g., if it's down)
      var localStrategy = new local_auth.Strategy(function(username, password, done) {
        // make sure this is an admin user
        process.nextTick(function() {
          var IncidentAdmin = dynamoose.model(tblpfx + 'IncidentAdmin', new dynamoose.Schema(schema.IncidentAdminSchemaDef));
          IncidentAdmin.query('user').eq(username).exec(function(err, admins) {
            if (err) {
              return done(err);
            } else {
              if (admins.length == 1) {
                return done(null, username);
              } else {
                return done(null, false, {
                  message: 'Invalid username'
                });
              }
            }
          });
        });
      });
      passport.use(localStrategy);

      // setup Duo strategy for admin users to bypass Shib (e.g., if it's down)
      var duoStrategy = new duo_auth.Strategy(duoIkey, duoSkey, duoHost, '/login-duo');
      passport.use(duoStrategy);

      ///////////////////////////////////////////////////////////////////////////////
      // login, login callback, and metadata routes
      //

      app.get(loginUrl, passport.authenticate(shibStrategy.name), uashib.backToUrl());
      app.post(loginCallbackUrl, passport.authenticate(shibStrategy.name, {
        failureRedirect: '/authErr',
        failureFlash: true
      }), uashib.backToUrl());
      app.get(uashib.urls.metadata, uashib.metadataRoute(shibStrategy, publicCert));

      //secure all routes following this
      //alternatively, you can use ensureAuth as middleware on specific routes
      //example:
      //  app.get('protected/resource', uashib.ensureAuth(loginUrl), function(req, res) {
      //      //route code
      //  });
      // app.use(uashib.ensureAuth(loginUrl));

      ///////////////////////////////////////////////////////////////////////////////
      // application routes
      // app.get('/', uashib.ensureAuth(loginUrl), routes.index);
      app.get('/', nocache, routes.index);
      app.get('/authErr', routes.authErr);
      // app.get('/partials/:name', uashib.ensureAuth(loginUrl), routes.partials);
      app.get('/partials/:name', nocache, routes.partials);
      app.get('/logout', function(req, res) {
        req.logout();
        req.session.destroy();
        res.redirect(uashib.urls.uaLogoutUrl);
      });

      // triggers Shib authentication
      app.get('/logon', uashib.ensureAuth(loginUrl), function(req, res) {
        res.redirect('/');
      });

      // handle admin username form submission
      app.post('/admin-login-submit',
        passport.authenticate('local', {
          failureRedirect: '/admin-login-redirect',
          failureFlash: true
        }),
        function(req, res) {
          res.redirect('/auth-duo');
        }
      );

      // redirect back to admin username form, with error message
      app.get('/admin-login-redirect',
        nocache,
        function(req, res) {
          res.redirect('/?route=/admin-login-err&message=' + req.flash('error'));
        }
      );

      // we redirect user here to trigger Duo auth sequence
      app.get('/auth-duo', nocache, loggedin.ensureLoggedIn('/admin-login'),
        passport.authenticate('duo', {
          postUrl: '/auth-duo',
          failureRedirect: '/auth-duo',
          failureFlash: true
        }),
        function(req, res, next) {
          return next();
        });

      // the Duo Passport strategy redirects user here to login via Duo
      app.get('/login-duo', loggedin.ensureLoggedIn('/admin-login'),
        function(req, res, next) {
          res.render('loginDuo', {
            user: req.user,
            host: req.query.host,
            post_action: req.query.post_action,
            sig_request: req.query.signed_request
          });
        });

      // Duo signed response is POSTed here
      app.post('/auth-duo',
        passport.authenticate('duo', {
          failureRedirect: '/auth-duo',
          failureFlash: true
        }),
        function(req, res) {
          req.session.secondFactor = 'duo';
          res.redirect('/');
        });

      // JSON API
      app.get('/api/incidentList', nocache, api.incidentList);
      app.get('/api/i/:id', nocache, api.incident);
      app.get('/api/subscriptions', nocache, api.subscriptions(snsTopicArn));
      app.post('/api/incident-update', api.incidentUpdate(bitlyApiKey, snsTopicArn, alertBaseUrl));
      app.post('/api/incident-add', api.incidentCreate(bitlyApiKey, snsTopicArn, alertBaseUrl));
      app.post('/api/incident-del', api.incidentDelete);
      app.post('/api/subscribe', api.subscribe(snsTopicArn));
      app.post('/api/unsubscribe', api.unsubscribe);

      app.use(function(req, res) {
        // res.writeHeader(200, {'Content-Type': 'text/html'});
        var isAuth = ('user' in req && typeof req.user == 'object' && 'Shib-uid' in req.user) ? true : false;
        res.render('index', {
          title: "UITS Incident Alert Manager",
          is_auth: isAuth
        });
      });

      //general error handler
      //if any route throws, this will be called
      app.use(function(err, req, res, next) {
        console.error(err.stack || err.message);
        res.send(500, 'Server Error! ' + err.message);
      });

      ///////////////////////////////////////////////////////////////////////////////
      // set up interval-based process to keep lists in sync
      sync_interval = 15000;
      setInterval(function() {
        scheduler.pollSQSForTwitter(sqsUrl, twitConsumerKey, twitConsumerSecret, twitAccessTokenKey, twitAccessTokenSecret);
      }, sync_interval);
      // GDW - 07/15/2016 - due to potential network unavailability, no longer tie admins to Grouper/EDS; they are now managed directly in DynamoDB
      // setInterval(function() {
      //   scheduler.syncAdmins(adminGroupName, ldapUrl, ldapBindDN, ldapBindPw, ldapSearchBase);
      // }, sync_interval);

      ///////////////////////////////////////////////////////////////////////////////
      // web server creation and startup
      //

      //create the HTTPS server and pass the express app as the handler
      // uncomment the following if handling SSL within Node

      // var httpsServer = https.createServer({
      //   key: privateKey,
      //   cert: publicCert
      // }, app);
      //
      // httpsServer.listen(httpsPort, function() {
      //   console.log('Listening for HTTPS requests on port %d', httpsServer.address().port)
      // });

      app.listen(app.get('port'));
    }
  }
);
