/*
 * GET home page.
 */

exports.index = function(req, res){
  var isAuth = ('user' in req && typeof req.user == 'object' && 'Shib-uid' in req.user) ? true : false;
  res.render('index',{ title:"UITS Incident Alert Manager", is_auth:isAuth });
};

exports.partials = function (req, res) {
  var name = req.params.name;
  res.render('partials/' + name);
};

exports.authErr = function(req, res){
  res.render('authErr',{ login_errors: req.flash('error') });
};
