/*
 * Serve JSON to our AngularJS client
 */

var _ = require('underscore');
var request = require('request');
var AWS = require('aws-sdk');
var dynamoose = require('dynamoose');
var uuid = require('node-uuid');
var shortid = require('shortid');
var ldap = require('ldapjs');
var async = require('async');
var sets = require('simplesets');
var schema = require('../schema');
var debug = require('debug')('api');

// our DynamoDB schema
var IncidentSchema = new dynamoose.Schema(schema.IncidentSchemaDef);
var IncidentUpdatesSchema = new dynamoose.Schema(schema.IncidentUpdatesSchemaDef);
var IncidentChangeLogSchema = new dynamoose.Schema(schema.IncidentChangeLogSchemaDef);
var IncidentSubscriptionSchema = new dynamoose.Schema(schema.IncidentSubscriptionSchemaDef);
var IncidentAdminSchema = new dynamoose.Schema(schema.IncidentAdminSchemaDef);

//====================
// GET routes
//====================

// return all incidents
exports.incidentList = function(req, res) {
  var user = getUserFromReq(req);
  var incidents = [];
  var response = {};

  debug("*** I am in incidentList");
  debug("req.user == " + JSON.stringify(req.user));
  async.waterfall([
      // determine if user is an admin
      function(callback) {
        return isUserAdmin(user, response, callback);
      },
      // lists which the authenticated user is owner of
      function(callback) {
        var i;
        var IncidentList = dynamoose.model(tblpfx + 'Incident', IncidentSchema);

        IncidentList.scan('status').in(['OPEN', 'CLOSED']).exec(function(err, incs) {
          if (err)
            callback(err);
          if (incs.length > 0)
            incidents = _.sortBy(incs, 'modify_time').reverse();
          callback(null, incidents);
        });
      }
    ],
    function(err, results) {
      if (err instanceof Error) {
        logger.error(err.name + " : " + err.message);
      }
      if (err) {
        req.session.error_flash = err.name + ": " + err.message;
        res.json(false);
      } else {
        response['incidents'] = incidents
        debug("*** incident list response == " + JSON.stringify(response));
        res.json(response);
      }
    });
};

// return single incident
exports.incident = function(req, res) {
  debug("** I am in incident()");
  var id = req.params.id;
  debug("** id = " + id);

  var user = getUserFromReq(req);
  var response = {};

  var IncidentUpdates = dynamoose.model(tblpfx + 'IncidentUpdates', IncidentUpdatesSchema);
  var IncidentChangeLog = dynamoose.model(tblpfx + 'IncidentChangeLog', IncidentChangeLogSchema);
  var Incident = dynamoose.model(tblpfx + 'Incident', IncidentSchema);

  async.waterfall([
      // determine if user is an admin
      function(callback) {
        return isUserAdmin(user, response, callback);
      },
      // lists which the authenticated user is owner of
      function(callback) {
        var i;

        Incident.get({
          'id': id
        }, function(err, inc) {
          if (err) {
            callback(err);
          } else {
            if (typeof inc != "undefined" && inc != null) {
              incident = inc;
              callback(null, incident);
            } else {
              callback("cannot find incident with id = " + id);
            }
          }
        });
      },
      function(incident, callback) {
        var updates = [];
        IncidentUpdates.query('id').eq(incident.id).exec(function(err, incupd) {
          if (err) {
            callback(err);
          } else {
            if (incupd.length > 0)
              updates = _.sortBy(incupd, 'create_time').reverse();
            incident.updates = updates;
            callback(null, incident);
          }
        });
      },
      function(incident, callback) {
        var audit = [];
        IncidentChangeLog.query('id').eq(incident.id).exec(function(err, incaudit) {
          if (err) {
            callback(err);
          } else {
            if (incaudit.length > 0)
              audit = _.sortBy(incaudit, 'create_time').reverse();
            incident.auditlog = audit;
            callback(null, incident);
          }
        });
      }
    ],
    function(err, incident) {
      if (err instanceof Error) {
        logger.error(err.name + " : " + err.message);
      }
      if (err) {
        req.session.error_flash = err.name + ": " + err.message;
        res.json(false);
      } else {
        response['incident'] = incident;
        res.json(response);
      }
    }
  );
};

// return all subscriptions
exports.subscriptions = function(snsTopicArn) {
  return function(req, res) {
    var user = getUserFromReq(req);
    var nextToken = false;

    debug("*** I am in subscriptions");
    if (user.length == 0) {
      res.json({
        subscriptions: [],
        isAuth: false
      });
    } else {
      async.waterfall([
          // lists which the authenticated user is owner of
          function(callback) {
            var i;
            var subs = [];
            var SubscriptionList = dynamoose.model(tblpfx + 'IncidentSubscription', IncidentSubscriptionSchema);

            SubscriptionList.query('netid').eq(user).exec(function(err, _subs) {
              if (err)
                callback(err);
              // if (_subs.length > 0)
              //   subs = _.sortBy(_subs, 'create_time').reverse();
              callback(null, _subs);
            });
          },
          function(_subs, callback) {
            var subsMap = {};
            var subCnt = 0;
            var loop = true;
            var sns = new AWS.SNS({
              region: 'us-east-1'
            });
            debug("*** subs init: " + JSON.stringify(_subs));

            _.each(_subs, function(item) {
              subsMap[item.endpoint_type + ":" + item.endpoint] = item;
              subCnt++;
            });
            debug("*** subsMap init: " + JSON.stringify(subsMap));
            async.doWhilst(
              function(callback) {
                // get full list of subscriptions to ARN and iterate/match...blech
                debug("*** Entering doWhilst...subsMap = " + JSON.stringify(subsMap));
                var params = {
                  TopicArn: snsTopicArn,
                };
                if (nextToken) {
                  params.NextToken = nextToken;
                }
                sns.listSubscriptionsByTopic(params,
                  function(err, data) {
                    if (err) {
                      debug("*** err = " + JSON.stringify(err));
                      debug("*** data = " + JSON.stringify(data));
                      return (err);
                    } else {
                      nextToken = ('NextToken' in data ? data.NextToken : false);
                      _.each(data.Subscriptions,
                        function(item) {
                          var key = item.Protocol.toUpperCase() + ':' + item.Endpoint;
                          debug("key = " + key);
                          debug("ARN = " + item.SubscriptionArn);
                          if (key in subsMap) {
                            subsMap[key].arn = item.SubscriptionArn;
                            debug("subsMap = " + JSON.stringify(subsMap));
                            subCnt--;
                          }
                        }
                      );
                      loop = (subCnt > 0 && nextToken);
                      return callback();
                    }
                  }
                );
                // debug("*** Leaving doWhilst...subsMap = " + JSON.stringify(subsMap));
              },
              function() {
                return loop;
              },
              function(err) {
                // no more records
                if (err) return callback(err);
                debug("*** Returning from doWhilst...subsMap = " + JSON.stringify(subsMap));
                return callback(null, subsMap);
              }
            );
          }
        ],
        function(err, _subsMap) {
          if (err) {
            logger.error((err instanceof Error) ? err.name + " : " + err.message : "Error : " + err);
            req.session.error_flash = err.name + ": " + err.message;
            res.json(false);
          } else {
            var subArr = [];
            _.each(_.keys(_subsMap), function(key) {
              subArr.push(_subsMap[key]);
            });
            debug("subArr before sort: " + JSON.stringify(subArr))
            subArr = _.sortBy(subArr, 'create_time').reverse();
            debug("subArr after sort: " + JSON.stringify(subArr))
            res.json({
              subscriptions: subArr,
              isAuth: true
            });
          }
        }
      );
    }
  }
}

//====================
// POST routes
//====================

// update incident
exports.incidentUpdate = function(bitlyApiKey, snsTopicArn, alertBaseUrl) {
  return function(req, res) {
    debug("*** I am in incidentUpdate");

    var id = req.body.id;

    debug("*** id = " + id);

    var updateFields = {};
    var statusUpdate = req.body.update;
    var sendAlert = req.body.send_alert;

    updateFields['status'] = req.body.status;
    updateFields['description'] = req.body.description;
    updateFields['services'] = req.body.services;
    updateFields['impact'] = req.body.impact;
    updateFields['planop'] = req.body.planop;
    updateFields['contact'] = req.body.contact;
    updateFields['timeframe'] = req.body.timeframe;
    updateFields['modify_time'] = new Date().getTime();

    var result = {};
    var user = getUserFromReq(req);

    var _incident;
    var Incident = dynamoose.model(tblpfx + 'Incident', IncidentSchema);
    var IncidentUpdates = dynamoose.model(tblpfx + 'IncidentUpdates', IncidentUpdatesSchema);
    var IncidentChangeLog = dynamoose.model(tblpfx + 'IncidentChangeLog', IncidentChangeLogSchema);

    // use named function for final waterfall callback, so we can short-circuit
    // notification step if send_alert == false
    var finalCallback = function(err) {
      if (err) {
        result.status = false;
        result.error = (err instanceof Error) ? err.name + " : " + err.message : "Error : " + err;
      }
      if (err instanceof Error) {
        logger.error(result.error);
      }

      res.json({
        result: result
      });
    };

    result.status = true;

    // update the Incident, audit the change, and apply any status updates
    async.waterfall(
      [
        // determine if user is an admin
        function(callback) {
          return isUserAdmin(user, result, callback);
        },
        // get our incident
        function(callback) {
          if (!result.isAdmin) {
            return callback(new Error("Authorization denied"));
          }
          debug("*** Inside first waterfall function...id = " + id);
          Incident.get({
              id: id
            },
            function(err, inc) {
              if (err) {
                return callback(err);
              } else {
                if (typeof inc != "undefined" && inc != null) {
                  _incident = inc;
                  debug("1. returning from get Incident...id = " + id);
                  return callback(null);
                } else {
                  return callback("cannot find incident with id = " + id);
                }
              }
            });
        },
        // if there are changes, update the incident
        function(callback) {
          var updateChanges = {};
          _.each(_.keys(updateFields).sort(),
            function(key) {
              debug("==> key: " + key + ", prev = " + _incident[key] + ", new = " + updateFields[key]);
              if (_incident[key] != updateFields[key]) {
                updateChanges[key] = updateFields[key];
              }
            });
          // check if modify_time is only change
          if (_.keys(updateChanges).length == 1) { // modify_time will always be changed
            if (statusUpdate.length == 0) { // was a status update added?
              return callback(null);
            }
          }
          // set modification time
          Incident.update({
            id: id
          }, {
            $PUT: updateChanges
          }, function(err) {
            if (err) {
              return callback(new Error('Could not update Incident with id ' + id + ' : ' + err.message));
            } else {
              return callback(null);
            }
          });
        },
        // audit the changes
        function(callback) {
          var updateAudit = [];
          _.each(_.keys(updateFields).sort(),
            function(key) {
              if (key != "modify_time" && _incident[key] != updateFields[key]) {
                updateAudit.push({
                  'field': key,
                  'prev_val': _incident[key],
                  'new_val': updateFields[key]
                });
              }
            });
          if (updateAudit.length > 0) {
            new IncidentChangeLog({
              id: id,
              changed_by: user,
              changes: updateAudit,
            }).save(
              function(err) {
                if (err) {
                  return callback(new Error('Could not audit Incident with id ' + id + ' : ' + err.message));
                } else {
                  debug("2. returning from get Incident audit...id = " + id);
                  return callback(null);
                }
              }
            );
          } else {
            return callback(null);
          }
        },
        // create status update, if one was passed
        function(callback) {
          if (statusUpdate.length > 0) {
            new IncidentUpdates({
              id: id,
              updated_by: user,
              text: statusUpdate
            }).save(
              function(err) {
                if (err) {
                  return callback(new Error('Could not create status update for Incident with id ' + id + ' : ' + err.message));
                } else {
                  return callback(null);
                }
              }
            );
          } else {
            return callback(null);
          }
        },
        // generate shortcut URL
        function(callback) {
          if (sendAlert != "yes") {
            // no alert to send, so let's exit prematurely
            return finalCallback(null);
          }
          if (_.keys(updateFields).length > 0 || statusUpdate.length > 0) {
            request({
              url: 'https://api-ssl.bitly.com/v4/shorten',
              headers: {
                Authorization: 'Bearer ' + bitlyApiKey
              },
              json: true,
              body: {
                long_url: alertBaseUrl + id
              },
              method: 'POST'
            }, function(error, response, body) {
              if (error || (response.statusCode != 200 && response.statusCode != 201)) {
                return callback(error);
              }
              // debug("bit.ly url shortener response = " + body);
              // var resJson = JSON.parse(body);
              debug("bit.ly short url object: " + body.link);
              return callback(null, body.link);
            });
          } else {
            return callback(null, "");
          }
        },
        function(shortUrl, callback) {
          if (_.keys(updateFields).length > 0 || statusUpdate.length > 0) {
            // construct SNS message
            var messageBody =
              (statusUpdate.length > 0 ? "UPDATE: " + statusUpdate + "\n\n" : "") +
              "Incident URL: " + shortUrl + "\n\n" +
              "Overview: " + ((_incident['description'] != updateFields['description']) ? updateFields['description'] : _incident['description']) + "\n\n" +
              "Status: " + ((_incident['status'] != updateFields['status']) ? updateFields['status'] : _incident['status']) + "\n\n" +
              "Services Affected: " + ((_incident['services'] != updateFields['services']) ? updateFields['services'] : _incident['services']) + "\n\n" +
              "Date and Time: " + ((_incident['timeframe'] != updateFields['timeframe']) ? updateFields['timeframe'] : _incident['timeframe']) + "\n\n" +
              "Impact to Campus: " + ((_incident['impact'] != updateFields['impact']) ? updateFields['impact'] : _incident['impact']) + "\n\n" +
              "Planned Operation: " + ((_incident['planop'] != updateFields['planop']) ? updateFields['planop'] : _incident['planop']) + "\n\n" +
              "Contact: " + ((_incident['contact'] != updateFields['contact']) ? updateFields['contact'] : _incident['contact']);
            var snsPublishJson = {
              TopicArn: snsTopicArn,
              Subject: "UPDATE - " + _incident['title'] + " : " + shortUrl,
              Message: messageBody
            };
            debug("SNS payload: " + JSON.stringify(snsPublishJson, null, 4));
            var sns = new AWS.SNS({
              region: 'us-east-1'
            });
            sns.publish(snsPublishJson,
              function(err, data) {
                if (err) {
                  debug("*** err = " + JSON.stringify(err));
                  debug("*** data = " + JSON.stringify(data));
                  return callback(new Error("Error submitting SNS message: " + err.message));
                } else {
                  return callback(null);
                }
              }
            );
          } else {
            return callback(null);
          }
        }
      ],
      finalCallback
    );
  }
}

// create a new incident
exports.incidentCreate = function(bitlyApiKey, snsTopicArn, alertBaseUrl) {
  return function(req, res) {
    var title = req.body.title,
      status = req.body.status,
      description = req.body.description,
      services = req.body.services,
      impact = req.body.impact,
      planop = req.body.planop,
      contact = req.body.contact,
      timeframe = req.body.timeframe,
      result = {
        'status': true
      };

    // user = req.user['Shib-uid'];
    var user = getUserFromReq(req);

    var Incident = dynamoose.model(tblpfx + 'Incident', IncidentSchema);

    async.waterfall([
        // determine if user is an admin
        function(callback) {
          return isUserAdmin(user, result, callback);
        },
        function(callback) {
          if (!result.isAdmin) {
            return callback(new Error("Authorization denied"));
          }
          // generate id
          var id = shortid.generate();
          // save Incident
          new Incident({
            id: id,
            title: title,
            status: status,
            description: description,
            services: services,
            impact: impact,
            planop: planop,
            contact: contact,
            timeframe: timeframe
          }).save(
            function(err) {
              if (err) {
                return callback(err);
              }
              return callback(null, id);
            }
          );
        },
        function(id, callback) {
          // generate bit.ly-shortened URL
          request({
            url: 'https://api-ssl.bitly.com/v4/shorten',
            headers: {
              Authorization: 'Bearer ' + bitlyApiKey
            },
            json: true,
            body: {
              long_url: alertBaseUrl + id
            },
            method: 'POST'
          }, function(error, response, body) {
            if (error || (response.statusCode != 200 && response.statusCode != 201)) {
              return callback(error);
            }
            // debug("bit.ly url shortener response = " + body);
            // var resJson = JSON.parse(body);
            debug("bit.ly short url object: " + body.link);
            return callback(null, body.link);
          });
        },
        function(shortUrl, callback) {
          // construct SNS message
          var messageBody =
            "Incident URL: " + shortUrl + "\n\n" +
            "Overview: " + description + "\n\n" +
            "Status: " + status + "\n\n" +
            "Services Affected: " + services + "\n\n" +
            "Date and Time: " + timeframe + "\n\n" +
            "Impact to Campus: " + impact + "\n\n" +
            "Planned Operation: " + planop + "\n\n" +
            "Contact: " + contact + "\n\n";
          var snsPublishJson = {
            TopicArn: snsTopicArn,
            Subject: title + " : " + shortUrl,
            Message: messageBody
          };
          debug("SNS payload: " + JSON.stringify(snsPublishJson, null, 4));
          var sns = new AWS.SNS({
            region: 'us-east-1'
          });
          sns.publish(snsPublishJson,
            function(err, data) {
              if (err) {
                debug("*** err = " + JSON.stringify(err));
                debug("*** data = " + JSON.stringify(data));
                return callback(new Error("Error submitting SNS message: " + err.message));
              } else {
                return callback(null);
              }
            }
          );
        }
      ],
      function(err) {
        if (err) {
          result.status = false;
          result.error = (err instanceof Error) ? err.name + " : " + err.message : "Error : " + err;
        }
        if (err instanceof Error) {
          logger.error(result.error);
        }
        res.json({
          result: result
        });
      }
    );
  }
}

// delete an incident
exports.incidentDelete = function(req, res) {
  var id = req.body.id;
  var updateFields = [];
  result = {};

  updateFields['status'] = "DELETED";
  result.status = true;

  var user = getUserFromReq(req);

  var Incident = dynamoose.model(tblpfx + 'Incident', IncidentSchema);
  var IncidentChangeLog = dynamoose.model(tblpfx + 'IncidentChangeLog', IncidentChangeLogSchema);

  // update the Incident, audit the change, and apply any status updates
  async.waterfall(
    [
      // determine if user is an admin
      function(callback) {
        return isUserAdmin(user, result, callback);
      },
      // get our incident
      function(callback) {
        debug("*** Inside first waterfall function...id = " + id);
        if (!result.isAdmin) {
          return callback(new Error("Authorization denied"));
        }
        Incident.get({
            id: id
          },
          function(err, inc) {
            if (err) {
              callback(err);
            } else {
              if (typeof inc != "undefined" && inc != null) {
                incident = inc;
                callback(null, incident);
              } else {
                callback("cannot find incident with id = " + id);
              }
            }
          });
      },
      // update the status field on the incident
      function(incident, callback) {
        var updateChanges = {};
        _.each(_.keys(updateFields).sort(),
          function(key) {
            debug("==> key: " + key + ", prev = " + incident[key] + ", new = " + updateFields[key]);
            if (incident[key] != updateFields[key]) {
              updateChanges[key] = updateFields[key];
            }
          });
        if (_.keys(updateChanges).length > 0) {
          Incident.update({
            id: id
          }, {
            $PUT: updateChanges
          }, function(err) {
            if (err) return callback(new Error('Could not update Incident with id' + id + ' : ' + err.message));
          });
        }
        return callback(null, incident);
      },
      // audit the changes
      function(incident, callback) {
        var updateAudit = [];
        _.each(_.keys(updateFields).sort(),
          function(key) {
            if (incident[key] != updateFields[key]) {
              updateAudit.push({
                'field': key,
                'prev_val': incident[key],
                'new_val': updateFields[key]
              });
            }
          });
        if (updateAudit.length > 0) {
          new IncidentChangeLog({
            id: id,
            changed_by: user,
            changes: updateAudit,
          }).save(
            function(err) {
              if (err)
                return callback(new Error('Could not audit Incident with id' + id + ' : ' + err.message));
            }
          );
        }
        return callback(null);
      }
    ],
    function(err) {
      if (err) {
        result.status = false;
        result.error = (err instanceof Error) ? err.name + " : " + err.message : "Error : " + err;
      }
      if (err instanceof Error) {
        logger.error(result.error);
      }
      res.json({
        result: result
      });
    });
};

// subscribe an endpoint
exports.subscribe = function(snsTopicArn) {
  return function(req, res) {
    var type = req.body.type,
      endpoint = req.body.endpoint,
      result = {
        'status': true
      };

    var user = getUserFromReq(req);

    var IncidentSubscription = dynamoose.model(tblpfx + 'IncidentSubscription', IncidentSubscriptionSchema);

    async.series([
        function(callback) {
          if (user.length == 0) {
            return callback(new Error("Not authenticated"));
          }
          IncidentSubscription.query('netid').eq(user).exec(function(err, incs) {
            if (err) {
              callback(err);
            } else {
              var has = false;
              if (incs.length > 0) {
                _.each(incs, function(item) {
                  if (item.endpoint_type == type && item.endpoint == endpoint) {
                    has = true;
                  }
                });
              }
              if (has)
                return callback("Endpoint already registered");
              return callback();
            }
          });
        },
        function(callback) {
          // call SNS subscribe endpoint
          var snsSubscribeJson = {
            Protocol: type.toLowerCase(),
            TopicArn: snsTopicArn,
            Endpoint: endpoint
          };
          debug("SNS payload: " + JSON.stringify(snsSubscribeJson, null, 4));
          var sns = new AWS.SNS({
            region: 'us-east-1'
          });
          sns.subscribe(snsSubscribeJson,
            function(err, data) {
              if (err) {
                debug("*** err = " + JSON.stringify(err));
                debug("*** data = " + JSON.stringify(data));
                return callback(new Error("subscribing endpoint : " + err.message));
              } else {
                return callback(null);
              }
            }
          );
        },
        function(callback) {
          // create IncidentSubscription
          new IncidentSubscription({
            netid: user,
            endpoint_type: type,
            endpoint: endpoint,
          }).save(
            function(err) {
              if (err) {
                return callback(err);
              }
              return callback(null);
            }
          );
        },
      ],
      function(err) {
        if (err) {
          result.status = false;
          result.error = (err instanceof Error) ? err.name + " : " + err.message : "Error : " + err;
        }
        if (err instanceof Error) {
          logger.error(result.error);
        }
        res.json({
          result: result
        });
      }
    );
  }
}

// unsubscribe an endpoint
exports.unsubscribe = function(req, res) {
  var subArn = req.body.arn,
    subId = req.body.sub_id,
    result = {
      'status': true
    };

  var user = getUserFromReq(req);

  var IncidentSubscription = dynamoose.model(tblpfx + 'IncidentSubscription', IncidentSubscriptionSchema);

  async.series([
      function(callback) {
        if (user.length == 0) {
          return callback(new Error("Not authenticated"));
        }
        if (typeof(subArn) == "undefined" || subArn.slice(0, 4) != "arn:") {
          return callback(null);
        }
        // call SNS unsubscribe endpoint
        var snsUnsubscribeJson = {
          SubscriptionArn: subArn,
        };
        debug("SNS payload: " + JSON.stringify(snsUnsubscribeJson, null, 4));
        var sns = new AWS.SNS({
          region: 'us-east-1'
        });
        sns.unsubscribe(snsUnsubscribeJson,
          function(err, data) {
            if (err) {
              debug("*** err = " + JSON.stringify(err));
              debug("*** data = " + JSON.stringify(data));
              if (err.statusCode == 404) { // subscription not found
                return callback(null);
              } else {
                return callback(err);
              }
            } else {
              return callback(null);
            }
          }
        );
      },
      function(callback) {
        // delete IncidentSubscription
        IncidentSubscription.delete({
            netid: user,
            sub_id: subId,
          },
          function(err) {
            if (err) {
              return callback(err);
            }
            return callback(null);
          }
        );
      },
    ],
    function(err) {
      if (err) {
        result.status = false;
        result.error = (err instanceof Error) ? err.name + " : " + err.message : "Error : " + err;
      }
      if (err instanceof Error) {
        logger.error(result.error);
      }
      res.json({
        result: result
      });
    }
  );
}

// get user from req object
function getUserFromReq(req) {
  return ('user' in req && typeof req.user == 'object' && 'Shib-uid' in req.user) ? req.user['Shib-uid'] : '';
}

// is user in admin group?
function isUserAdmin(user, response, callback) {
  var IncidentAdmin = dynamoose.model(tblpfx + 'IncidentAdmin', IncidentAdminSchema);

  if (user.length == 0) {
    response['isAdmin'] = false;
    return callback(null);
  }

  IncidentAdmin.query('user').eq(user).exec(function(err, hits) {
    if (err) {
      return callback(err);
    } else {
      debug("isadmin results == " + JSON.stringify(hits));
      response['isAdmin'] = (hits.length == 1) ? true : false;
      return callback(null);
    }
  });
}
