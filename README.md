UITS Incident Alerts Manager
============================

This application uses [Node.js] + [Express] for the backend, and [AngularJS] on the frontend, with incident data stored in [AWS DynamoDB][DynamoDB]. It allows authorized users to create new incidents, and push alerts via [AWS SNS][Amazon SNS] to subscribed users and to a pre-defined [Twitter][Twitter Developers] account. Critical application errors are logged to an [AWS CloudWatch][Amazon CloudWatch] log group stream.

## How to use

### Prepare your environment

Clone the [uits-incident-manager](https://bitbucket.org/ua-uits-eas/uits-incident-manager) repository

You will need to install some Node.js module dependencies:

    npm install

Set the following environment variables to appropriate values:

    AWS_REGION=us-west-2 # or other valid region
    AWS_ACCESS_KEY_ID=<your AWS Access Key ID>
    AWS_SECRET_ACCESS_KEY=<your AWS Secret Access Key>
    ENTITYID=https://incidents.uits.arizona.edu/shibboleth # Shibboleth Entity ID
    DOMAIN=incidents.uits.arizona.edu # domain of URL to this app
    ALERTBASEURL=https://${DOMAIN}/i/ # base URL for generating links to alerts
    ADMINGROUP="arizona.edu:services:enterprise:incident-alerts:admins" # Grouper group containing admins
    LDAPURL=ldaps://eds.arizona.edu # LDAP URL of EDS Server
    LDAPSEARCHBASE="ou=people,dc=eds,dc=arizona,dc=edu"  # Base DN for searches
    LDAPBINDDN="uid=uits-incident-alerts,ou=App Users,dc=eds,dc=arizona,dc=edu" # bind DN
    LDAPBINDPW=itsasecret # bind password
    BITLYAPIKEY=<bit.ly API Key> # bit.ly API key that has access to the bit.ly URL Shortener service
    TWITTERCONSUMERKEY=<twitter consumer key> # consumer key for Twitter account
    TWITTERCONSUMERSECRET=<twitter consumer secret> # secret key for Twitter consumer key
    TWITTERACCESSTOKENKEY=<twitter access token key> # access token key for Twitter account
    TWITTERACCESSTOKENSECRET=<twitter access token secret> # secret for Twitter access token key
    DECRYPTKEY='<base64-encoded secret key>' # used to decrypt Shib and session secrets
    AWSCWLOGSTRM=uits-incident-alert-log-stream # AWS CloudWatch Log Stream
    AWSCWLOGGRP=uits-incident-alert-log # AWS CloudWatch Log Group
    AWSSNSTOPICARN=arn:aws:sns:us-east-1:XXXXXXXXXXXX:UITS-Alerts # AWS SNS Topic ARN for alerts (must be in us-east-1 for SMS)
    AWSSQSURL=https://sqs.us-west-2.amazonaws.com/XXXXXXXXXXXX/UITSIncidents # URL of AWS SQS queue (used for Twitter)
    S3BUCKET=mys3bucket # S3 bucket containing Shib and session secrets
    S3FOLDER=mys3folder # if using a folder within S3 bucket, specify here; omit, otherwise
    DYNAMODBTBLPREFIX=uits-incidents- # string that will be prepended to DynamoDB table names
    DUOIKEY=<Duo app iKey> # Duo IKEY (used for admin login when Shibboleth is down)
    DUOSKEY=<Duo app sKey> # Duo SKEY (used for admin login when Shibboleth is down)
    DUOHOST=api-XXXXXXXX.duosecurity.com # Duo API hostname
    DEBUG=http,api,scheduler # components for which we wish to log debug statements to Node console

### Running the app

Runs like a typical Express app:

    node app.js

Or you can run it this way to automatically reload any changes you make

    nodemon app.js

## References

* [AngularJS][AngularJS]
* [AWS DynamoDB][DynamoDB]
* [AWS SNS][Amazon SNS]
* [AWS CloudWatch][Amazon CloudWatch]
* [Express][Express]
* [Node.js][Node.js]
* [Twitter][Twitter Developers]

[EDS]: http://sia.uits.arizona.edu/eds "EDS"
[AngularJS]: http://angularjs.org/ "AngularJS"
[DynamoDB]: http://aws.amazon.com/dynamodb/ "Amazon DynamoDB"
[Amazon SNS]: https://aws.amazon.com/sns/ "Amazon SNS"
[Amazon CloudWatch]: https://aws.amazon.com/cloudwatch/ "Amazon CloudWatch"
[Express]: http://expressjs.com/ "Express"
[Node.js]: http://nodejs.org "Node.js"
[Twitter Developers]: https://dev.twitter.com "Twitter Developers"
